FROM gradle:5.6.2-jdk8
WORKDIR /app

ADD build.gradle /app/build.gradle
ADD src /app/src

RUN gradle shadowJar
CMD java -Xms2g -Xmx16g -jar /app/build/libs/wurcs2pic.jar


