```
$ gradle dependencies

> Task :dependencies

------------------------------------------------------------
Root project
------------------------------------------------------------

annotationProcessor - Annotation processors and their dependencies for source set 'main'.
No dependencies

apiElements - API elements for main. (n)
No dependencies

archives - Configuration for archive artifacts. (n)
No dependencies

compileClasspath - Compile classpath for source set 'main'.
+--- com.google.guava:guava:28.0-jre
|    +--- com.google.guava:failureaccess:1.0.1
|    +--- com.google.guava:listenablefuture:9999.0-empty-to-avoid-conflict-with-guava
|    +--- com.google.code.findbugs:jsr305:3.0.2
|    +--- org.checkerframework:checker-qual:2.8.1
|    +--- com.google.errorprone:error_prone_annotations:2.3.2
|    +--- com.google.j2objc:j2objc-annotations:1.3
|    \--- org.codehaus.mojo:animal-sniffer-annotations:1.17
+--- com.sparkjava:spark-core:2.8.0
|    +--- org.slf4j:slf4j-api:1.7.25
|    +--- org.eclipse.jetty:jetty-server:9.4.12.v20180830
|    |    +--- javax.servlet:javax.servlet-api:3.1.0
|    |    +--- org.eclipse.jetty:jetty-http:9.4.12.v20180830
|    |    |    +--- org.eclipse.jetty:jetty-util:9.4.12.v20180830
|    |    |    \--- org.eclipse.jetty:jetty-io:9.4.12.v20180830
|    |    |         \--- org.eclipse.jetty:jetty-util:9.4.12.v20180830
|    |    \--- org.eclipse.jetty:jetty-io:9.4.12.v20180830 (*)
|    +--- org.eclipse.jetty:jetty-webapp:9.4.12.v20180830
|    |    +--- org.eclipse.jetty:jetty-xml:9.4.12.v20180830
|    |    |    \--- org.eclipse.jetty:jetty-util:9.4.12.v20180830
|    |    \--- org.eclipse.jetty:jetty-servlet:9.4.12.v20180830
|    |         \--- org.eclipse.jetty:jetty-security:9.4.12.v20180830
|    |              \--- org.eclipse.jetty:jetty-server:9.4.12.v20180830 (*)
|    +--- org.eclipse.jetty.websocket:websocket-server:9.4.12.v20180830
|    |    +--- org.eclipse.jetty.websocket:websocket-common:9.4.12.v20180830
|    |    |    +--- org.eclipse.jetty.websocket:websocket-api:9.4.12.v20180830
|    |    |    +--- org.eclipse.jetty:jetty-util:9.4.12.v20180830
|    |    |    \--- org.eclipse.jetty:jetty-io:9.4.12.v20180830 (*)
|    |    +--- org.eclipse.jetty.websocket:websocket-client:9.4.12.v20180830
|    |    |    +--- org.eclipse.jetty:jetty-client:9.4.12.v20180830
|    |    |    |    +--- org.eclipse.jetty:jetty-http:9.4.12.v20180830 (*)
|    |    |    |    \--- org.eclipse.jetty:jetty-io:9.4.12.v20180830 (*)
|    |    |    +--- org.eclipse.jetty:jetty-xml:9.4.12.v20180830 (*)
|    |    |    +--- org.eclipse.jetty:jetty-util:9.4.12.v20180830
|    |    |    +--- org.eclipse.jetty:jetty-io:9.4.12.v20180830 (*)
|    |    |    \--- org.eclipse.jetty.websocket:websocket-common:9.4.12.v20180830 (*)
|    |    +--- org.eclipse.jetty.websocket:websocket-servlet:9.4.12.v20180830
|    |    |    +--- org.eclipse.jetty.websocket:websocket-api:9.4.12.v20180830
|    |    |    \--- javax.servlet:javax.servlet-api:3.1.0
|    |    +--- org.eclipse.jetty:jetty-servlet:9.4.12.v20180830 (*)
|    |    \--- org.eclipse.jetty:jetty-http:9.4.12.v20180830 (*)
|    \--- org.eclipse.jetty.websocket:websocket-servlet:9.4.12.v20180830 (*)
+--- com.google.code.gson:gson:2.8.0
+--- commons-codec:commons-codec:1.10
\--- org.eurocarbdb.glycanbuilder:wurcsextend:1.0.5-SNAPSHOT
     +--- org.glycoinfo:wurcsframework:1.0.1
     |    +--- org.slf4j:slf4j-api:1.7.25
     |    \--- ch.qos.logback:logback-classic:1.2.3
     |         +--- ch.qos.logback:logback-core:1.2.3
     |         \--- org.slf4j:slf4j-api:1.7.25
     +--- org.eurocarbdb:MolecularFramework:0.1-b37-SNAPSHOT
     |    +--- org.eurocarbdb:resourcesdb:0.3 -> 0.4
     |    |    +--- org.jdom:jdom:1.1.3
     |    |    +--- org.eclipse.birt.runtime.3_7_1:org.apache.batik.dom.svg:1.6.0
     |    |    +--- org.eclipse.birt.runtime.3_7_1:org.apache.batik.svggen:1.6.0
     |    |    +--- org.eclipse.birt.runtime.3_7_1:org.apache.batik.transcoder:1.6.0
     |    |    +--- org.eclipse.birt.runtime.3_7_1:org.w3c.dom.svg:1.1.0
     |    |    +--- org.eclipse.birt.runtime.3_7_1:org.apache.batik.ext.awt:1.6.0
     |    |    +--- batik:batik-swing:1.6-1
     |    |    |    \--- batik:batik-bridge:1.6-1
     |    |    |         +--- batik:batik-gvt:1.6-1
     |    |    |         |    \--- batik:batik-awt-util:1.6-1
     |    |    |         |         \--- batik:batik-util:1.6-1
     |    |    |         |              \--- batik:batik-gui-util:1.6-1
     |    |    |         |                   \--- batik:batik-ext:1.6-1
     |    |    |         |                        \--- xml-apis:xmlParserAPIs:2.0.2
     |    |    |         +--- batik:batik-script:1.6-1
     |    |    |         |    +--- batik:batik-bridge:1.6-1 (*)
     |    |    |         |    +--- batik:batik-svg-dom:1.6-1
     |    |    |         |    |    +--- batik:batik-dom:1.6-1
     |    |    |         |    |    |    +--- batik:batik-css:1.6-1
     |    |    |         |    |    |    |    \--- batik:batik-util:1.6-1 (*)
     |    |    |         |    |    |    +--- batik:batik-xml:1.6-1
     |    |    |         |    |    |    |    \--- batik:batik-util:1.6-1 (*)
     |    |    |         |    |    |    \--- xerces:xercesImpl:2.5.0
     |    |    |         |    |    \--- batik:batik-parser:1.6-1
     |    |    |         |    |         \--- batik:batik-awt-util:1.6-1 (*)
     |    |    |         |    +--- batik:batik-gvt:1.6-1 (*)
     |    |    |         |    \--- rhino:js:1.5R4.1
     |    |    |         \--- batik:batik-svg-dom:1.6-1 (*)
     |    |    \--- com.opensymphony:xwork:2.1.3
     |    |         +--- opensymphony:ognl:2.6.11
     |    |         \--- org.springframework:spring-test:2.5.6
     |    |              +--- commons-logging:commons-logging:1.1.1
     |    |              \--- junit:junit:3.8.1
     |    +--- org.apache.logging.log4j:log4j-api:2.2
     |    +--- org.apache.logging.log4j:log4j-core:2.2
     |    |    \--- org.apache.logging.log4j:log4j-api:2.2
     |    \--- org.glycomedb:residuetranslator:0.1-b12
     +--- org.glycoinfo.eurocarbdb.depends:flamingo:0.1
     +--- org.apache.xmlgraphics:fop:2.0
     |    +--- org.apache.xmlgraphics:xmlgraphics-commons:2.0.1
     |    |    +--- commons-io:commons-io:1.3.1
     |    |    \--- commons-logging:commons-logging:1.0.4 -> 1.1.1
     |    +--- org.apache.xmlgraphics:batik-svg-dom:1.8
     |    |    +--- org.apache.xmlgraphics:batik-awt-util:1.8
     |    |    |    \--- org.apache.xmlgraphics:batik-util:1.8
     |    |    +--- org.apache.xmlgraphics:batik-css:1.8
     |    |    |    +--- org.apache.xmlgraphics:batik-ext:1.8
     |    |    |    |    \--- xml-apis:xml-apis:1.3.04 -> 1.4.01
     |    |    |    +--- org.apache.xmlgraphics:batik-util:1.8
     |    |    |    +--- xml-apis:xml-apis:1.3.04 -> 1.4.01
     |    |    |    \--- xml-apis:xml-apis-ext:1.3.04
     |    |    +--- org.apache.xmlgraphics:batik-dom:1.8
     |    |    |    +--- org.apache.xmlgraphics:batik-css:1.8 (*)
     |    |    |    +--- org.apache.xmlgraphics:batik-ext:1.8 (*)
     |    |    |    +--- org.apache.xmlgraphics:batik-util:1.8
     |    |    |    +--- org.apache.xmlgraphics:batik-xml:1.8
     |    |    |    |    \--- org.apache.xmlgraphics:batik-util:1.8
     |    |    |    +--- xalan:xalan:2.7.0
     |    |    |    |    \--- xml-apis:xml-apis:2.0.2 -> 1.4.01
     |    |    |    +--- xml-apis:xml-apis:1.3.04 -> 1.4.01
     |    |    |    \--- xml-apis:xml-apis-ext:1.3.04
     |    |    +--- org.apache.xmlgraphics:batik-ext:1.8 (*)
     |    |    +--- org.apache.xmlgraphics:batik-parser:1.8
     |    |    |    +--- org.apache.xmlgraphics:batik-awt-util:1.8 (*)
     |    |    |    +--- org.apache.xmlgraphics:batik-util:1.8
     |    |    |    +--- org.apache.xmlgraphics:batik-xml:1.8 (*)
     |    |    |    \--- xml-apis:xml-apis:1.3.04 -> 1.4.01
     |    |    +--- org.apache.xmlgraphics:batik-util:1.8
     |    |    +--- xml-apis:xml-apis:1.3.04 -> 1.4.01
     |    |    \--- xml-apis:xml-apis-ext:1.3.04
     |    +--- org.apache.xmlgraphics:batik-bridge:1.8
     |    |    +--- org.apache.xmlgraphics:batik-anim:1.8
     |    |    |    +--- org.apache.xmlgraphics:batik-awt-util:1.8 (*)
     |    |    |    +--- org.apache.xmlgraphics:batik-css:1.8 (*)
     |    |    |    +--- org.apache.xmlgraphics:batik-dom:1.8 (*)
     |    |    |    +--- org.apache.xmlgraphics:batik-ext:1.8 (*)
     |    |    |    +--- org.apache.xmlgraphics:batik-parser:1.8 (*)
     |    |    |    +--- org.apache.xmlgraphics:batik-svg-dom:1.8 (*)
     |    |    |    +--- org.apache.xmlgraphics:batik-util:1.8
     |    |    |    +--- xml-apis:xml-apis:1.3.04 -> 1.4.01
     |    |    |    \--- xml-apis:xml-apis-ext:1.3.04
     |    |    +--- org.apache.xmlgraphics:batik-awt-util:1.8 (*)
     |    |    +--- org.apache.xmlgraphics:batik-css:1.8 (*)
     |    |    +--- org.apache.xmlgraphics:batik-dom:1.8 (*)
     |    |    +--- org.apache.xmlgraphics:batik-ext:1.8 (*)
     |    |    +--- org.apache.xmlgraphics:batik-gvt:1.8
     |    |    |    +--- org.apache.xmlgraphics:batik-awt-util:1.8 (*)
     |    |    |    \--- org.apache.xmlgraphics:batik-util:1.8
     |    |    +--- org.apache.xmlgraphics:batik-parser:1.8 (*)
     |    |    +--- org.apache.xmlgraphics:batik-script:1.8
     |    |    |    +--- org.apache.xmlgraphics:batik-anim:1.8 (*)
     |    |    |    +--- org.apache.xmlgraphics:batik-util:1.8
     |    |    |    \--- xml-apis:xml-apis:1.3.04 -> 1.4.01
     |    |    +--- org.apache.xmlgraphics:batik-svg-dom:1.8 (*)
     |    |    +--- org.apache.xmlgraphics:batik-util:1.8
     |    |    +--- org.apache.xmlgraphics:batik-xml:1.8 (*)
     |    |    +--- xalan:xalan:2.7.0 (*)
     |    |    +--- xml-apis:xml-apis:1.3.04 -> 1.4.01
     |    |    \--- xml-apis:xml-apis-ext:1.3.04
     |    +--- org.apache.xmlgraphics:batik-awt-util:1.8 (*)
     |    +--- org.apache.xmlgraphics:batik-gvt:1.8 (*)
     |    +--- org.apache.xmlgraphics:batik-transcoder:1.8
     |    |    +--- org.apache.xmlgraphics:batik-anim:1.8 (*)
     |    |    +--- org.apache.xmlgraphics:batik-awt-util:1.8 (*)
     |    |    +--- org.apache.xmlgraphics:batik-bridge:1.8 (*)
     |    |    +--- org.apache.xmlgraphics:batik-dom:1.8 (*)
     |    |    +--- org.apache.xmlgraphics:batik-gvt:1.8 (*)
     |    |    +--- org.apache.xmlgraphics:batik-svggen:1.8
     |    |    |    +--- org.apache.xmlgraphics:batik-awt-util:1.8 (*)
     |    |    |    +--- org.apache.xmlgraphics:batik-util:1.8
     |    |    |    \--- xml-apis:xml-apis:1.3.04 -> 1.4.01
     |    |    +--- org.apache.xmlgraphics:batik-util:1.8
     |    |    +--- org.apache.xmlgraphics:batik-xml:1.8 (*)
     |    |    +--- xml-apis:xml-apis:1.3.04 -> 1.4.01
     |    |    \--- xml-apis:xml-apis-ext:1.3.04
     |    +--- org.apache.xmlgraphics:batik-extension:1.8
     |    |    +--- org.apache.xmlgraphics:batik-anim:1.8 (*)
     |    |    +--- org.apache.xmlgraphics:batik-awt-util:1.8 (*)
     |    |    +--- org.apache.xmlgraphics:batik-bridge:1.8 (*)
     |    |    +--- org.apache.xmlgraphics:batik-css:1.8 (*)
     |    |    +--- org.apache.xmlgraphics:batik-dom:1.8 (*)
     |    |    +--- org.apache.xmlgraphics:batik-ext:1.8 (*)
     |    |    +--- org.apache.xmlgraphics:batik-gvt:1.8 (*)
     |    |    +--- org.apache.xmlgraphics:batik-parser:1.8 (*)
     |    |    +--- org.apache.xmlgraphics:batik-svg-dom:1.8 (*)
     |    |    +--- org.apache.xmlgraphics:batik-util:1.8
     |    |    +--- xml-apis:xml-apis:1.3.04 -> 1.4.01
     |    |    \--- xml-apis:xml-apis-ext:1.3.04
     |    +--- org.apache.xmlgraphics:batik-ext:1.8 (*)
     |    +--- commons-logging:commons-logging:1.0.4 -> 1.1.1
     |    +--- commons-io:commons-io:1.3.1
     |    +--- avalon-framework:avalon-framework-api:4.2.0
     |    \--- avalon-framework:avalon-framework-impl:4.2.0
     +--- jdom:jdom:1.1
     |    \--- org.jdom:jdom:1.1 -> 1.1.3
     +--- log4j:log4j:1.2.17
     +--- net.java.dev.swing-layout:swing-layout:1.0.2
     +--- com.jgoodies:jgoodies-looks:2.7.0
     |    \--- com.jgoodies:jgoodies-common:1.8.1
     +--- org.glycoinfo.eurocarbdb.depends:DJNativeSwing-SWT:0.2
     +--- org.glycoinfo.eurocarbdb.depends:DJNativeSwing:0.1
     +--- org.glycoinfo.eurocarbdb.depends:flamingo-tst:0.1
     +--- org.eurocarbdb:resourcesdb:0.4 (*)
     +--- org.glycoinfo.convert:glycanformatconverter:2.5.2
     |    +--- commons-collections:commons-collections:20040616
     |    +--- org.json:json:20180813
     |    +--- org.eurocarbdb:MolecularFramework:0.1-b37-SNAPSHOT (*)
     |    \--- org.glycoinfo:wurcsframework:1.0.1 (*)
     \--- org.apache.ant:ant-launcher:1.9.7

compileOnly - Compile only dependencies for source set 'main'. (n)
No dependencies

default - Configuration for default artifacts. (n)
No dependencies

implementation - Implementation only dependencies for source set 'main'. (n)
+--- com.google.guava:guava:28.0-jre (n)
+--- com.sparkjava:spark-core:2.8.0 (n)
+--- com.google.code.gson:gson:2.8.0 (n)
+--- commons-codec:commons-codec:1.10 (n)
\--- org.eurocarbdb.glycanbuilder:wurcsextend:1.0.5-SNAPSHOT (n)

runtimeClasspath - Runtime classpath of source set 'main'.
+--- com.google.guava:guava:28.0-jre
|    +--- com.google.guava:failureaccess:1.0.1
|    +--- com.google.guava:listenablefuture:9999.0-empty-to-avoid-conflict-with-guava
|    +--- com.google.code.findbugs:jsr305:3.0.2
|    +--- org.checkerframework:checker-qual:2.8.1
|    +--- com.google.errorprone:error_prone_annotations:2.3.2
|    +--- com.google.j2objc:j2objc-annotations:1.3
|    \--- org.codehaus.mojo:animal-sniffer-annotations:1.17
+--- com.sparkjava:spark-core:2.8.0
|    +--- org.slf4j:slf4j-api:1.7.25
|    +--- org.eclipse.jetty:jetty-server:9.4.12.v20180830
|    |    +--- javax.servlet:javax.servlet-api:3.1.0
|    |    +--- org.eclipse.jetty:jetty-http:9.4.12.v20180830
|    |    |    +--- org.eclipse.jetty:jetty-util:9.4.12.v20180830
|    |    |    \--- org.eclipse.jetty:jetty-io:9.4.12.v20180830
|    |    |         \--- org.eclipse.jetty:jetty-util:9.4.12.v20180830
|    |    \--- org.eclipse.jetty:jetty-io:9.4.12.v20180830 (*)
|    +--- org.eclipse.jetty:jetty-webapp:9.4.12.v20180830
|    |    +--- org.eclipse.jetty:jetty-xml:9.4.12.v20180830
|    |    |    \--- org.eclipse.jetty:jetty-util:9.4.12.v20180830
|    |    \--- org.eclipse.jetty:jetty-servlet:9.4.12.v20180830
|    |         \--- org.eclipse.jetty:jetty-security:9.4.12.v20180830
|    |              \--- org.eclipse.jetty:jetty-server:9.4.12.v20180830 (*)
|    +--- org.eclipse.jetty.websocket:websocket-server:9.4.12.v20180830
|    |    +--- org.eclipse.jetty.websocket:websocket-common:9.4.12.v20180830
|    |    |    +--- org.eclipse.jetty.websocket:websocket-api:9.4.12.v20180830
|    |    |    +--- org.eclipse.jetty:jetty-util:9.4.12.v20180830
|    |    |    \--- org.eclipse.jetty:jetty-io:9.4.12.v20180830 (*)
|    |    +--- org.eclipse.jetty.websocket:websocket-client:9.4.12.v20180830
|    |    |    +--- org.eclipse.jetty:jetty-client:9.4.12.v20180830
|    |    |    |    +--- org.eclipse.jetty:jetty-http:9.4.12.v20180830 (*)
|    |    |    |    \--- org.eclipse.jetty:jetty-io:9.4.12.v20180830 (*)
|    |    |    +--- org.eclipse.jetty:jetty-xml:9.4.12.v20180830 (*)
|    |    |    +--- org.eclipse.jetty:jetty-util:9.4.12.v20180830
|    |    |    +--- org.eclipse.jetty:jetty-io:9.4.12.v20180830 (*)
|    |    |    \--- org.eclipse.jetty.websocket:websocket-common:9.4.12.v20180830 (*)
|    |    +--- org.eclipse.jetty.websocket:websocket-servlet:9.4.12.v20180830
|    |    |    +--- org.eclipse.jetty.websocket:websocket-api:9.4.12.v20180830
|    |    |    \--- javax.servlet:javax.servlet-api:3.1.0
|    |    +--- org.eclipse.jetty:jetty-servlet:9.4.12.v20180830 (*)
|    |    \--- org.eclipse.jetty:jetty-http:9.4.12.v20180830 (*)
|    \--- org.eclipse.jetty.websocket:websocket-servlet:9.4.12.v20180830 (*)
+--- com.google.code.gson:gson:2.8.0
+--- commons-codec:commons-codec:1.10
\--- org.eurocarbdb.glycanbuilder:wurcsextend:1.0.5-SNAPSHOT
     +--- org.glycoinfo:wurcsframework:1.0.1
     |    +--- org.slf4j:slf4j-api:1.7.25
     |    \--- ch.qos.logback:logback-classic:1.2.3
     |         +--- ch.qos.logback:logback-core:1.2.3
     |         \--- org.slf4j:slf4j-api:1.7.25
     +--- org.eurocarbdb:MolecularFramework:0.1-b37-SNAPSHOT
     |    +--- org.eurocarbdb:resourcesdb:0.3 -> 0.4
     |    |    +--- org.jdom:jdom:1.1.3
     |    |    +--- org.eclipse.birt.runtime.3_7_1:org.apache.batik.dom.svg:1.6.0
     |    |    +--- org.eclipse.birt.runtime.3_7_1:org.apache.batik.svggen:1.6.0
     |    |    +--- org.eclipse.birt.runtime.3_7_1:org.apache.batik.transcoder:1.6.0
     |    |    +--- org.eclipse.birt.runtime.3_7_1:org.w3c.dom.svg:1.1.0
     |    |    +--- org.eclipse.birt.runtime.3_7_1:org.apache.batik.ext.awt:1.6.0
     |    |    +--- batik:batik-swing:1.6-1
     |    |    |    \--- batik:batik-bridge:1.6-1
     |    |    |         +--- batik:batik-gvt:1.6-1
     |    |    |         |    \--- batik:batik-awt-util:1.6-1
     |    |    |         |         \--- batik:batik-util:1.6-1
     |    |    |         |              \--- batik:batik-gui-util:1.6-1
     |    |    |         |                   \--- batik:batik-ext:1.6-1
     |    |    |         |                        \--- xml-apis:xmlParserAPIs:2.0.2
     |    |    |         +--- batik:batik-script:1.6-1
     |    |    |         |    +--- batik:batik-bridge:1.6-1 (*)
     |    |    |         |    +--- batik:batik-svg-dom:1.6-1
     |    |    |         |    |    +--- batik:batik-dom:1.6-1
     |    |    |         |    |    |    +--- batik:batik-css:1.6-1
     |    |    |         |    |    |    |    \--- batik:batik-util:1.6-1 (*)
     |    |    |         |    |    |    +--- batik:batik-xml:1.6-1
     |    |    |         |    |    |    |    \--- batik:batik-util:1.6-1 (*)
     |    |    |         |    |    |    \--- xerces:xercesImpl:2.5.0
     |    |    |         |    |    \--- batik:batik-parser:1.6-1
     |    |    |         |    |         \--- batik:batik-awt-util:1.6-1 (*)
     |    |    |         |    +--- batik:batik-gvt:1.6-1 (*)
     |    |    |         |    \--- rhino:js:1.5R4.1
     |    |    |         \--- batik:batik-svg-dom:1.6-1 (*)
     |    |    \--- com.opensymphony:xwork:2.1.3
     |    |         +--- opensymphony:ognl:2.6.11
     |    |         \--- org.springframework:spring-test:2.5.6
     |    |              +--- commons-logging:commons-logging:1.1.1
     |    |              \--- junit:junit:3.8.1
     |    +--- org.apache.logging.log4j:log4j-api:2.2
     |    +--- org.apache.logging.log4j:log4j-core:2.2
     |    |    \--- org.apache.logging.log4j:log4j-api:2.2
     |    \--- org.glycomedb:residuetranslator:0.1-b12
     +--- org.glycoinfo.eurocarbdb.depends:flamingo:0.1
     +--- org.apache.xmlgraphics:fop:2.0
     |    +--- org.apache.xmlgraphics:xmlgraphics-commons:2.0.1
     |    |    +--- commons-io:commons-io:1.3.1
     |    |    \--- commons-logging:commons-logging:1.0.4 -> 1.1.1
     |    +--- org.apache.xmlgraphics:batik-svg-dom:1.8
     |    |    +--- org.apache.xmlgraphics:batik-awt-util:1.8
     |    |    |    \--- org.apache.xmlgraphics:batik-util:1.8
     |    |    +--- org.apache.xmlgraphics:batik-css:1.8
     |    |    |    +--- org.apache.xmlgraphics:batik-ext:1.8
     |    |    |    |    \--- xml-apis:xml-apis:1.3.04 -> 1.4.01
     |    |    |    +--- org.apache.xmlgraphics:batik-util:1.8
     |    |    |    +--- xml-apis:xml-apis:1.3.04 -> 1.4.01
     |    |    |    \--- xml-apis:xml-apis-ext:1.3.04
     |    |    +--- org.apache.xmlgraphics:batik-dom:1.8
     |    |    |    +--- org.apache.xmlgraphics:batik-css:1.8 (*)
     |    |    |    +--- org.apache.xmlgraphics:batik-ext:1.8 (*)
     |    |    |    +--- org.apache.xmlgraphics:batik-util:1.8
     |    |    |    +--- org.apache.xmlgraphics:batik-xml:1.8
     |    |    |    |    \--- org.apache.xmlgraphics:batik-util:1.8
     |    |    |    +--- xalan:xalan:2.7.0
     |    |    |    |    \--- xml-apis:xml-apis:2.0.2 -> 1.4.01
     |    |    |    +--- xml-apis:xml-apis:1.3.04 -> 1.4.01
     |    |    |    \--- xml-apis:xml-apis-ext:1.3.04
     |    |    +--- org.apache.xmlgraphics:batik-ext:1.8 (*)
     |    |    +--- org.apache.xmlgraphics:batik-parser:1.8
     |    |    |    +--- org.apache.xmlgraphics:batik-awt-util:1.8 (*)
     |    |    |    +--- org.apache.xmlgraphics:batik-util:1.8
     |    |    |    +--- org.apache.xmlgraphics:batik-xml:1.8 (*)
     |    |    |    \--- xml-apis:xml-apis:1.3.04 -> 1.4.01
     |    |    +--- org.apache.xmlgraphics:batik-util:1.8
     |    |    +--- xml-apis:xml-apis:1.3.04 -> 1.4.01
     |    |    \--- xml-apis:xml-apis-ext:1.3.04
     |    +--- org.apache.xmlgraphics:batik-bridge:1.8
     |    |    +--- org.apache.xmlgraphics:batik-anim:1.8
     |    |    |    +--- org.apache.xmlgraphics:batik-awt-util:1.8 (*)
     |    |    |    +--- org.apache.xmlgraphics:batik-css:1.8 (*)
     |    |    |    +--- org.apache.xmlgraphics:batik-dom:1.8 (*)
     |    |    |    +--- org.apache.xmlgraphics:batik-ext:1.8 (*)
     |    |    |    +--- org.apache.xmlgraphics:batik-parser:1.8 (*)
     |    |    |    +--- org.apache.xmlgraphics:batik-svg-dom:1.8 (*)
     |    |    |    +--- org.apache.xmlgraphics:batik-util:1.8
     |    |    |    +--- xml-apis:xml-apis:1.3.04 -> 1.4.01
     |    |    |    \--- xml-apis:xml-apis-ext:1.3.04
     |    |    +--- org.apache.xmlgraphics:batik-awt-util:1.8 (*)
     |    |    +--- org.apache.xmlgraphics:batik-css:1.8 (*)
     |    |    +--- org.apache.xmlgraphics:batik-dom:1.8 (*)
     |    |    +--- org.apache.xmlgraphics:batik-ext:1.8 (*)
     |    |    +--- org.apache.xmlgraphics:batik-gvt:1.8
     |    |    |    +--- org.apache.xmlgraphics:batik-awt-util:1.8 (*)
     |    |    |    \--- org.apache.xmlgraphics:batik-util:1.8
     |    |    +--- org.apache.xmlgraphics:batik-parser:1.8 (*)
     |    |    +--- org.apache.xmlgraphics:batik-script:1.8
     |    |    |    +--- org.apache.xmlgraphics:batik-anim:1.8 (*)
     |    |    |    +--- org.apache.xmlgraphics:batik-util:1.8
     |    |    |    \--- xml-apis:xml-apis:1.3.04 -> 1.4.01
     |    |    +--- org.apache.xmlgraphics:batik-svg-dom:1.8 (*)
     |    |    +--- org.apache.xmlgraphics:batik-util:1.8
     |    |    +--- org.apache.xmlgraphics:batik-xml:1.8 (*)
     |    |    +--- xalan:xalan:2.7.0 (*)
     |    |    +--- xml-apis:xml-apis:1.3.04 -> 1.4.01
     |    |    \--- xml-apis:xml-apis-ext:1.3.04
     |    +--- org.apache.xmlgraphics:batik-awt-util:1.8 (*)
     |    +--- org.apache.xmlgraphics:batik-gvt:1.8 (*)
     |    +--- org.apache.xmlgraphics:batik-transcoder:1.8
     |    |    +--- org.apache.xmlgraphics:batik-anim:1.8 (*)
     |    |    +--- org.apache.xmlgraphics:batik-awt-util:1.8 (*)
     |    |    +--- org.apache.xmlgraphics:batik-bridge:1.8 (*)
     |    |    +--- org.apache.xmlgraphics:batik-dom:1.8 (*)
     |    |    +--- org.apache.xmlgraphics:batik-gvt:1.8 (*)
     |    |    +--- org.apache.xmlgraphics:batik-svggen:1.8
     |    |    |    +--- org.apache.xmlgraphics:batik-awt-util:1.8 (*)
     |    |    |    +--- org.apache.xmlgraphics:batik-util:1.8
     |    |    |    \--- xml-apis:xml-apis:1.3.04 -> 1.4.01
     |    |    +--- org.apache.xmlgraphics:batik-util:1.8
     |    |    +--- org.apache.xmlgraphics:batik-xml:1.8 (*)
     |    |    +--- xml-apis:xml-apis:1.3.04 -> 1.4.01
     |    |    \--- xml-apis:xml-apis-ext:1.3.04
     |    +--- org.apache.xmlgraphics:batik-extension:1.8
     |    |    +--- org.apache.xmlgraphics:batik-anim:1.8 (*)
     |    |    +--- org.apache.xmlgraphics:batik-awt-util:1.8 (*)
     |    |    +--- org.apache.xmlgraphics:batik-bridge:1.8 (*)
     |    |    +--- org.apache.xmlgraphics:batik-css:1.8 (*)
     |    |    +--- org.apache.xmlgraphics:batik-dom:1.8 (*)
     |    |    +--- org.apache.xmlgraphics:batik-ext:1.8 (*)
     |    |    +--- org.apache.xmlgraphics:batik-gvt:1.8 (*)
     |    |    +--- org.apache.xmlgraphics:batik-parser:1.8 (*)
     |    |    +--- org.apache.xmlgraphics:batik-svg-dom:1.8 (*)
     |    |    +--- org.apache.xmlgraphics:batik-util:1.8
     |    |    +--- xml-apis:xml-apis:1.3.04 -> 1.4.01
     |    |    \--- xml-apis:xml-apis-ext:1.3.04
     |    +--- org.apache.xmlgraphics:batik-ext:1.8 (*)
     |    +--- commons-logging:commons-logging:1.0.4 -> 1.1.1
     |    +--- commons-io:commons-io:1.3.1
     |    +--- avalon-framework:avalon-framework-api:4.2.0
     |    \--- avalon-framework:avalon-framework-impl:4.2.0
     +--- jdom:jdom:1.1
     |    \--- org.jdom:jdom:1.1 -> 1.1.3
     +--- log4j:log4j:1.2.17
     +--- net.java.dev.swing-layout:swing-layout:1.0.2
     +--- com.jgoodies:jgoodies-looks:2.7.0
     |    \--- com.jgoodies:jgoodies-common:1.8.1
     +--- org.glycoinfo.eurocarbdb.depends:DJNativeSwing-SWT:0.2
     +--- org.glycoinfo.eurocarbdb.depends:DJNativeSwing:0.1
     +--- org.glycoinfo.eurocarbdb.depends:flamingo-tst:0.1
     +--- org.eurocarbdb:resourcesdb:0.4 (*)
     +--- org.glycoinfo.convert:glycanformatconverter:2.5.2
     |    +--- commons-collections:commons-collections:20040616
     |    +--- org.json:json:20180813
     |    +--- org.eurocarbdb:MolecularFramework:0.1-b37-SNAPSHOT (*)
     |    \--- org.glycoinfo:wurcsframework:1.0.1 (*)
     \--- org.apache.ant:ant-launcher:1.9.7

runtimeElements - Elements of runtime for main. (n)
No dependencies

runtimeOnly - Runtime only dependencies for source set 'main'. (n)
No dependencies

shadow
No dependencies

testAnnotationProcessor - Annotation processors and their dependencies for source set 'test'.
No dependencies

testCompileClasspath - Compile classpath for source set 'test'.
+--- com.google.guava:guava:28.0-jre
|    +--- com.google.guava:failureaccess:1.0.1
|    +--- com.google.guava:listenablefuture:9999.0-empty-to-avoid-conflict-with-guava
|    +--- com.google.code.findbugs:jsr305:3.0.2
|    +--- org.checkerframework:checker-qual:2.8.1
|    +--- com.google.errorprone:error_prone_annotations:2.3.2
|    +--- com.google.j2objc:j2objc-annotations:1.3
|    \--- org.codehaus.mojo:animal-sniffer-annotations:1.17
+--- com.sparkjava:spark-core:2.8.0
|    +--- org.slf4j:slf4j-api:1.7.25
|    +--- org.eclipse.jetty:jetty-server:9.4.12.v20180830
|    |    +--- javax.servlet:javax.servlet-api:3.1.0
|    |    +--- org.eclipse.jetty:jetty-http:9.4.12.v20180830
|    |    |    +--- org.eclipse.jetty:jetty-util:9.4.12.v20180830
|    |    |    \--- org.eclipse.jetty:jetty-io:9.4.12.v20180830
|    |    |         \--- org.eclipse.jetty:jetty-util:9.4.12.v20180830
|    |    \--- org.eclipse.jetty:jetty-io:9.4.12.v20180830 (*)
|    +--- org.eclipse.jetty:jetty-webapp:9.4.12.v20180830
|    |    +--- org.eclipse.jetty:jetty-xml:9.4.12.v20180830
|    |    |    \--- org.eclipse.jetty:jetty-util:9.4.12.v20180830
|    |    \--- org.eclipse.jetty:jetty-servlet:9.4.12.v20180830
|    |         \--- org.eclipse.jetty:jetty-security:9.4.12.v20180830
|    |              \--- org.eclipse.jetty:jetty-server:9.4.12.v20180830 (*)
|    +--- org.eclipse.jetty.websocket:websocket-server:9.4.12.v20180830
|    |    +--- org.eclipse.jetty.websocket:websocket-common:9.4.12.v20180830
|    |    |    +--- org.eclipse.jetty.websocket:websocket-api:9.4.12.v20180830
|    |    |    +--- org.eclipse.jetty:jetty-util:9.4.12.v20180830
|    |    |    \--- org.eclipse.jetty:jetty-io:9.4.12.v20180830 (*)
|    |    +--- org.eclipse.jetty.websocket:websocket-client:9.4.12.v20180830
|    |    |    +--- org.eclipse.jetty:jetty-client:9.4.12.v20180830
|    |    |    |    +--- org.eclipse.jetty:jetty-http:9.4.12.v20180830 (*)
|    |    |    |    \--- org.eclipse.jetty:jetty-io:9.4.12.v20180830 (*)
|    |    |    +--- org.eclipse.jetty:jetty-xml:9.4.12.v20180830 (*)
|    |    |    +--- org.eclipse.jetty:jetty-util:9.4.12.v20180830
|    |    |    +--- org.eclipse.jetty:jetty-io:9.4.12.v20180830 (*)
|    |    |    \--- org.eclipse.jetty.websocket:websocket-common:9.4.12.v20180830 (*)
|    |    +--- org.eclipse.jetty.websocket:websocket-servlet:9.4.12.v20180830
|    |    |    +--- org.eclipse.jetty.websocket:websocket-api:9.4.12.v20180830
|    |    |    \--- javax.servlet:javax.servlet-api:3.1.0
|    |    +--- org.eclipse.jetty:jetty-servlet:9.4.12.v20180830 (*)
|    |    \--- org.eclipse.jetty:jetty-http:9.4.12.v20180830 (*)
|    \--- org.eclipse.jetty.websocket:websocket-servlet:9.4.12.v20180830 (*)
+--- com.google.code.gson:gson:2.8.0
+--- commons-codec:commons-codec:1.10
+--- org.eurocarbdb.glycanbuilder:wurcsextend:1.0.5-SNAPSHOT
|    +--- org.glycoinfo:wurcsframework:1.0.1
|    |    +--- org.slf4j:slf4j-api:1.7.25
|    |    \--- ch.qos.logback:logback-classic:1.2.3
|    |         +--- ch.qos.logback:logback-core:1.2.3
|    |         \--- org.slf4j:slf4j-api:1.7.25
|    +--- org.eurocarbdb:MolecularFramework:0.1-b37-SNAPSHOT
|    |    +--- org.eurocarbdb:resourcesdb:0.3 -> 0.4
|    |    |    +--- org.jdom:jdom:1.1.3
|    |    |    +--- org.eclipse.birt.runtime.3_7_1:org.apache.batik.dom.svg:1.6.0
|    |    |    +--- org.eclipse.birt.runtime.3_7_1:org.apache.batik.svggen:1.6.0
|    |    |    +--- org.eclipse.birt.runtime.3_7_1:org.apache.batik.transcoder:1.6.0
|    |    |    +--- org.eclipse.birt.runtime.3_7_1:org.w3c.dom.svg:1.1.0
|    |    |    +--- org.eclipse.birt.runtime.3_7_1:org.apache.batik.ext.awt:1.6.0
|    |    |    +--- batik:batik-swing:1.6-1
|    |    |    |    \--- batik:batik-bridge:1.6-1
|    |    |    |         +--- batik:batik-gvt:1.6-1
|    |    |    |         |    \--- batik:batik-awt-util:1.6-1
|    |    |    |         |         \--- batik:batik-util:1.6-1
|    |    |    |         |              \--- batik:batik-gui-util:1.6-1
|    |    |    |         |                   \--- batik:batik-ext:1.6-1
|    |    |    |         |                        \--- xml-apis:xmlParserAPIs:2.0.2
|    |    |    |         +--- batik:batik-script:1.6-1
|    |    |    |         |    +--- batik:batik-bridge:1.6-1 (*)
|    |    |    |         |    +--- batik:batik-svg-dom:1.6-1
|    |    |    |         |    |    +--- batik:batik-dom:1.6-1
|    |    |    |         |    |    |    +--- batik:batik-css:1.6-1
|    |    |    |         |    |    |    |    \--- batik:batik-util:1.6-1 (*)
|    |    |    |         |    |    |    +--- batik:batik-xml:1.6-1
|    |    |    |         |    |    |    |    \--- batik:batik-util:1.6-1 (*)
|    |    |    |         |    |    |    \--- xerces:xercesImpl:2.5.0
|    |    |    |         |    |    \--- batik:batik-parser:1.6-1
|    |    |    |         |    |         \--- batik:batik-awt-util:1.6-1 (*)
|    |    |    |         |    +--- batik:batik-gvt:1.6-1 (*)
|    |    |    |         |    \--- rhino:js:1.5R4.1
|    |    |    |         \--- batik:batik-svg-dom:1.6-1 (*)
|    |    |    \--- com.opensymphony:xwork:2.1.3
|    |    |         +--- opensymphony:ognl:2.6.11
|    |    |         \--- org.springframework:spring-test:2.5.6
|    |    |              +--- commons-logging:commons-logging:1.1.1
|    |    |              \--- junit:junit:3.8.1 -> 4.12
|    |    |                   \--- org.hamcrest:hamcrest-core:1.3
|    |    +--- org.apache.logging.log4j:log4j-api:2.2
|    |    +--- org.apache.logging.log4j:log4j-core:2.2
|    |    |    \--- org.apache.logging.log4j:log4j-api:2.2
|    |    \--- org.glycomedb:residuetranslator:0.1-b12
|    +--- org.glycoinfo.eurocarbdb.depends:flamingo:0.1
|    +--- org.apache.xmlgraphics:fop:2.0
|    |    +--- org.apache.xmlgraphics:xmlgraphics-commons:2.0.1
|    |    |    +--- commons-io:commons-io:1.3.1
|    |    |    \--- commons-logging:commons-logging:1.0.4 -> 1.1.1
|    |    +--- org.apache.xmlgraphics:batik-svg-dom:1.8
|    |    |    +--- org.apache.xmlgraphics:batik-awt-util:1.8
|    |    |    |    \--- org.apache.xmlgraphics:batik-util:1.8
|    |    |    +--- org.apache.xmlgraphics:batik-css:1.8
|    |    |    |    +--- org.apache.xmlgraphics:batik-ext:1.8
|    |    |    |    |    \--- xml-apis:xml-apis:1.3.04 -> 1.4.01
|    |    |    |    +--- org.apache.xmlgraphics:batik-util:1.8
|    |    |    |    +--- xml-apis:xml-apis:1.3.04 -> 1.4.01
|    |    |    |    \--- xml-apis:xml-apis-ext:1.3.04
|    |    |    +--- org.apache.xmlgraphics:batik-dom:1.8
|    |    |    |    +--- org.apache.xmlgraphics:batik-css:1.8 (*)
|    |    |    |    +--- org.apache.xmlgraphics:batik-ext:1.8 (*)
|    |    |    |    +--- org.apache.xmlgraphics:batik-util:1.8
|    |    |    |    +--- org.apache.xmlgraphics:batik-xml:1.8
|    |    |    |    |    \--- org.apache.xmlgraphics:batik-util:1.8
|    |    |    |    +--- xalan:xalan:2.7.0
|    |    |    |    |    \--- xml-apis:xml-apis:2.0.2 -> 1.4.01
|    |    |    |    +--- xml-apis:xml-apis:1.3.04 -> 1.4.01
|    |    |    |    \--- xml-apis:xml-apis-ext:1.3.04
|    |    |    +--- org.apache.xmlgraphics:batik-ext:1.8 (*)
|    |    |    +--- org.apache.xmlgraphics:batik-parser:1.8
|    |    |    |    +--- org.apache.xmlgraphics:batik-awt-util:1.8 (*)
|    |    |    |    +--- org.apache.xmlgraphics:batik-util:1.8
|    |    |    |    +--- org.apache.xmlgraphics:batik-xml:1.8 (*)
|    |    |    |    \--- xml-apis:xml-apis:1.3.04 -> 1.4.01
|    |    |    +--- org.apache.xmlgraphics:batik-util:1.8
|    |    |    +--- xml-apis:xml-apis:1.3.04 -> 1.4.01
|    |    |    \--- xml-apis:xml-apis-ext:1.3.04
|    |    +--- org.apache.xmlgraphics:batik-bridge:1.8
|    |    |    +--- org.apache.xmlgraphics:batik-anim:1.8
|    |    |    |    +--- org.apache.xmlgraphics:batik-awt-util:1.8 (*)
|    |    |    |    +--- org.apache.xmlgraphics:batik-css:1.8 (*)
|    |    |    |    +--- org.apache.xmlgraphics:batik-dom:1.8 (*)
|    |    |    |    +--- org.apache.xmlgraphics:batik-ext:1.8 (*)
|    |    |    |    +--- org.apache.xmlgraphics:batik-parser:1.8 (*)
|    |    |    |    +--- org.apache.xmlgraphics:batik-svg-dom:1.8 (*)
|    |    |    |    +--- org.apache.xmlgraphics:batik-util:1.8
|    |    |    |    +--- xml-apis:xml-apis:1.3.04 -> 1.4.01
|    |    |    |    \--- xml-apis:xml-apis-ext:1.3.04
|    |    |    +--- org.apache.xmlgraphics:batik-awt-util:1.8 (*)
|    |    |    +--- org.apache.xmlgraphics:batik-css:1.8 (*)
|    |    |    +--- org.apache.xmlgraphics:batik-dom:1.8 (*)
|    |    |    +--- org.apache.xmlgraphics:batik-ext:1.8 (*)
|    |    |    +--- org.apache.xmlgraphics:batik-gvt:1.8
|    |    |    |    +--- org.apache.xmlgraphics:batik-awt-util:1.8 (*)
|    |    |    |    \--- org.apache.xmlgraphics:batik-util:1.8
|    |    |    +--- org.apache.xmlgraphics:batik-parser:1.8 (*)
|    |    |    +--- org.apache.xmlgraphics:batik-script:1.8
|    |    |    |    +--- org.apache.xmlgraphics:batik-anim:1.8 (*)
|    |    |    |    +--- org.apache.xmlgraphics:batik-util:1.8
|    |    |    |    \--- xml-apis:xml-apis:1.3.04 -> 1.4.01
|    |    |    +--- org.apache.xmlgraphics:batik-svg-dom:1.8 (*)
|    |    |    +--- org.apache.xmlgraphics:batik-util:1.8
|    |    |    +--- org.apache.xmlgraphics:batik-xml:1.8 (*)
|    |    |    +--- xalan:xalan:2.7.0 (*)
|    |    |    +--- xml-apis:xml-apis:1.3.04 -> 1.4.01
|    |    |    \--- xml-apis:xml-apis-ext:1.3.04
|    |    +--- org.apache.xmlgraphics:batik-awt-util:1.8 (*)
|    |    +--- org.apache.xmlgraphics:batik-gvt:1.8 (*)
|    |    +--- org.apache.xmlgraphics:batik-transcoder:1.8
|    |    |    +--- org.apache.xmlgraphics:batik-anim:1.8 (*)
|    |    |    +--- org.apache.xmlgraphics:batik-awt-util:1.8 (*)
|    |    |    +--- org.apache.xmlgraphics:batik-bridge:1.8 (*)
|    |    |    +--- org.apache.xmlgraphics:batik-dom:1.8 (*)
|    |    |    +--- org.apache.xmlgraphics:batik-gvt:1.8 (*)
|    |    |    +--- org.apache.xmlgraphics:batik-svggen:1.8
|    |    |    |    +--- org.apache.xmlgraphics:batik-awt-util:1.8 (*)
|    |    |    |    +--- org.apache.xmlgraphics:batik-util:1.8
|    |    |    |    \--- xml-apis:xml-apis:1.3.04 -> 1.4.01
|    |    |    +--- org.apache.xmlgraphics:batik-util:1.8
|    |    |    +--- org.apache.xmlgraphics:batik-xml:1.8 (*)
|    |    |    +--- xml-apis:xml-apis:1.3.04 -> 1.4.01
|    |    |    \--- xml-apis:xml-apis-ext:1.3.04
|    |    +--- org.apache.xmlgraphics:batik-extension:1.8
|    |    |    +--- org.apache.xmlgraphics:batik-anim:1.8 (*)
|    |    |    +--- org.apache.xmlgraphics:batik-awt-util:1.8 (*)
|    |    |    +--- org.apache.xmlgraphics:batik-bridge:1.8 (*)
|    |    |    +--- org.apache.xmlgraphics:batik-css:1.8 (*)
|    |    |    +--- org.apache.xmlgraphics:batik-dom:1.8 (*)
|    |    |    +--- org.apache.xmlgraphics:batik-ext:1.8 (*)
|    |    |    +--- org.apache.xmlgraphics:batik-gvt:1.8 (*)
|    |    |    +--- org.apache.xmlgraphics:batik-parser:1.8 (*)
|    |    |    +--- org.apache.xmlgraphics:batik-svg-dom:1.8 (*)
|    |    |    +--- org.apache.xmlgraphics:batik-util:1.8
|    |    |    +--- xml-apis:xml-apis:1.3.04 -> 1.4.01
|    |    |    \--- xml-apis:xml-apis-ext:1.3.04
|    |    +--- org.apache.xmlgraphics:batik-ext:1.8 (*)
|    |    +--- commons-logging:commons-logging:1.0.4 -> 1.1.1
|    |    +--- commons-io:commons-io:1.3.1
|    |    +--- avalon-framework:avalon-framework-api:4.2.0
|    |    \--- avalon-framework:avalon-framework-impl:4.2.0
|    +--- jdom:jdom:1.1
|    |    \--- org.jdom:jdom:1.1 -> 1.1.3
|    +--- log4j:log4j:1.2.17
|    +--- net.java.dev.swing-layout:swing-layout:1.0.2
|    +--- com.jgoodies:jgoodies-looks:2.7.0
|    |    \--- com.jgoodies:jgoodies-common:1.8.1
|    +--- org.glycoinfo.eurocarbdb.depends:DJNativeSwing-SWT:0.2
|    +--- org.glycoinfo.eurocarbdb.depends:DJNativeSwing:0.1
|    +--- org.glycoinfo.eurocarbdb.depends:flamingo-tst:0.1
|    +--- org.eurocarbdb:resourcesdb:0.4 (*)
|    +--- org.glycoinfo.convert:glycanformatconverter:2.5.2
|    |    +--- commons-collections:commons-collections:20040616
|    |    +--- org.json:json:20180813
|    |    +--- org.eurocarbdb:MolecularFramework:0.1-b37-SNAPSHOT (*)
|    |    \--- org.glycoinfo:wurcsframework:1.0.1 (*)
|    \--- org.apache.ant:ant-launcher:1.9.7
\--- junit:junit:4.12 (*)

testCompileOnly - Compile only dependencies for source set 'test'. (n)
No dependencies

testImplementation - Implementation only dependencies for source set 'test'. (n)
\--- junit:junit:4.12 (n)

testRuntimeClasspath - Runtime classpath of source set 'test'.
+--- com.google.guava:guava:28.0-jre
|    +--- com.google.guava:failureaccess:1.0.1
|    +--- com.google.guava:listenablefuture:9999.0-empty-to-avoid-conflict-with-guava
|    +--- com.google.code.findbugs:jsr305:3.0.2
|    +--- org.checkerframework:checker-qual:2.8.1
|    +--- com.google.errorprone:error_prone_annotations:2.3.2
|    +--- com.google.j2objc:j2objc-annotations:1.3
|    \--- org.codehaus.mojo:animal-sniffer-annotations:1.17
+--- com.sparkjava:spark-core:2.8.0
|    +--- org.slf4j:slf4j-api:1.7.25
|    +--- org.eclipse.jetty:jetty-server:9.4.12.v20180830
|    |    +--- javax.servlet:javax.servlet-api:3.1.0
|    |    +--- org.eclipse.jetty:jetty-http:9.4.12.v20180830
|    |    |    +--- org.eclipse.jetty:jetty-util:9.4.12.v20180830
|    |    |    \--- org.eclipse.jetty:jetty-io:9.4.12.v20180830
|    |    |         \--- org.eclipse.jetty:jetty-util:9.4.12.v20180830
|    |    \--- org.eclipse.jetty:jetty-io:9.4.12.v20180830 (*)
|    +--- org.eclipse.jetty:jetty-webapp:9.4.12.v20180830
|    |    +--- org.eclipse.jetty:jetty-xml:9.4.12.v20180830
|    |    |    \--- org.eclipse.jetty:jetty-util:9.4.12.v20180830
|    |    \--- org.eclipse.jetty:jetty-servlet:9.4.12.v20180830
|    |         \--- org.eclipse.jetty:jetty-security:9.4.12.v20180830
|    |              \--- org.eclipse.jetty:jetty-server:9.4.12.v20180830 (*)
|    +--- org.eclipse.jetty.websocket:websocket-server:9.4.12.v20180830
|    |    +--- org.eclipse.jetty.websocket:websocket-common:9.4.12.v20180830
|    |    |    +--- org.eclipse.jetty.websocket:websocket-api:9.4.12.v20180830
|    |    |    +--- org.eclipse.jetty:jetty-util:9.4.12.v20180830
|    |    |    \--- org.eclipse.jetty:jetty-io:9.4.12.v20180830 (*)
|    |    +--- org.eclipse.jetty.websocket:websocket-client:9.4.12.v20180830
|    |    |    +--- org.eclipse.jetty:jetty-client:9.4.12.v20180830
|    |    |    |    +--- org.eclipse.jetty:jetty-http:9.4.12.v20180830 (*)
|    |    |    |    \--- org.eclipse.jetty:jetty-io:9.4.12.v20180830 (*)
|    |    |    +--- org.eclipse.jetty:jetty-xml:9.4.12.v20180830 (*)
|    |    |    +--- org.eclipse.jetty:jetty-util:9.4.12.v20180830
|    |    |    +--- org.eclipse.jetty:jetty-io:9.4.12.v20180830 (*)
|    |    |    \--- org.eclipse.jetty.websocket:websocket-common:9.4.12.v20180830 (*)
|    |    +--- org.eclipse.jetty.websocket:websocket-servlet:9.4.12.v20180830
|    |    |    +--- org.eclipse.jetty.websocket:websocket-api:9.4.12.v20180830
|    |    |    \--- javax.servlet:javax.servlet-api:3.1.0
|    |    +--- org.eclipse.jetty:jetty-servlet:9.4.12.v20180830 (*)
|    |    \--- org.eclipse.jetty:jetty-http:9.4.12.v20180830 (*)
|    \--- org.eclipse.jetty.websocket:websocket-servlet:9.4.12.v20180830 (*)
+--- com.google.code.gson:gson:2.8.0
+--- commons-codec:commons-codec:1.10
+--- org.eurocarbdb.glycanbuilder:wurcsextend:1.0.5-SNAPSHOT
|    +--- org.glycoinfo:wurcsframework:1.0.1
|    |    +--- org.slf4j:slf4j-api:1.7.25
|    |    \--- ch.qos.logback:logback-classic:1.2.3
|    |         +--- ch.qos.logback:logback-core:1.2.3
|    |         \--- org.slf4j:slf4j-api:1.7.25
|    +--- org.eurocarbdb:MolecularFramework:0.1-b37-SNAPSHOT
|    |    +--- org.eurocarbdb:resourcesdb:0.3 -> 0.4
|    |    |    +--- org.jdom:jdom:1.1.3
|    |    |    +--- org.eclipse.birt.runtime.3_7_1:org.apache.batik.dom.svg:1.6.0
|    |    |    +--- org.eclipse.birt.runtime.3_7_1:org.apache.batik.svggen:1.6.0
|    |    |    +--- org.eclipse.birt.runtime.3_7_1:org.apache.batik.transcoder:1.6.0
|    |    |    +--- org.eclipse.birt.runtime.3_7_1:org.w3c.dom.svg:1.1.0
|    |    |    +--- org.eclipse.birt.runtime.3_7_1:org.apache.batik.ext.awt:1.6.0
|    |    |    +--- batik:batik-swing:1.6-1
|    |    |    |    \--- batik:batik-bridge:1.6-1
|    |    |    |         +--- batik:batik-gvt:1.6-1
|    |    |    |         |    \--- batik:batik-awt-util:1.6-1
|    |    |    |         |         \--- batik:batik-util:1.6-1
|    |    |    |         |              \--- batik:batik-gui-util:1.6-1
|    |    |    |         |                   \--- batik:batik-ext:1.6-1
|    |    |    |         |                        \--- xml-apis:xmlParserAPIs:2.0.2
|    |    |    |         +--- batik:batik-script:1.6-1
|    |    |    |         |    +--- batik:batik-bridge:1.6-1 (*)
|    |    |    |         |    +--- batik:batik-svg-dom:1.6-1
|    |    |    |         |    |    +--- batik:batik-dom:1.6-1
|    |    |    |         |    |    |    +--- batik:batik-css:1.6-1
|    |    |    |         |    |    |    |    \--- batik:batik-util:1.6-1 (*)
|    |    |    |         |    |    |    +--- batik:batik-xml:1.6-1
|    |    |    |         |    |    |    |    \--- batik:batik-util:1.6-1 (*)
|    |    |    |         |    |    |    \--- xerces:xercesImpl:2.5.0
|    |    |    |         |    |    \--- batik:batik-parser:1.6-1
|    |    |    |         |    |         \--- batik:batik-awt-util:1.6-1 (*)
|    |    |    |         |    +--- batik:batik-gvt:1.6-1 (*)
|    |    |    |         |    \--- rhino:js:1.5R4.1
|    |    |    |         \--- batik:batik-svg-dom:1.6-1 (*)
|    |    |    \--- com.opensymphony:xwork:2.1.3
|    |    |         +--- opensymphony:ognl:2.6.11
|    |    |         \--- org.springframework:spring-test:2.5.6
|    |    |              +--- commons-logging:commons-logging:1.1.1
|    |    |              \--- junit:junit:3.8.1 -> 4.12
|    |    |                   \--- org.hamcrest:hamcrest-core:1.3
|    |    +--- org.apache.logging.log4j:log4j-api:2.2
|    |    +--- org.apache.logging.log4j:log4j-core:2.2
|    |    |    \--- org.apache.logging.log4j:log4j-api:2.2
|    |    \--- org.glycomedb:residuetranslator:0.1-b12
|    +--- org.glycoinfo.eurocarbdb.depends:flamingo:0.1
|    +--- org.apache.xmlgraphics:fop:2.0
|    |    +--- org.apache.xmlgraphics:xmlgraphics-commons:2.0.1
|    |    |    +--- commons-io:commons-io:1.3.1
|    |    |    \--- commons-logging:commons-logging:1.0.4 -> 1.1.1
|    |    +--- org.apache.xmlgraphics:batik-svg-dom:1.8
|    |    |    +--- org.apache.xmlgraphics:batik-awt-util:1.8
|    |    |    |    \--- org.apache.xmlgraphics:batik-util:1.8
|    |    |    +--- org.apache.xmlgraphics:batik-css:1.8
|    |    |    |    +--- org.apache.xmlgraphics:batik-ext:1.8
|    |    |    |    |    \--- xml-apis:xml-apis:1.3.04 -> 1.4.01
|    |    |    |    +--- org.apache.xmlgraphics:batik-util:1.8
|    |    |    |    +--- xml-apis:xml-apis:1.3.04 -> 1.4.01
|    |    |    |    \--- xml-apis:xml-apis-ext:1.3.04
|    |    |    +--- org.apache.xmlgraphics:batik-dom:1.8
|    |    |    |    +--- org.apache.xmlgraphics:batik-css:1.8 (*)
|    |    |    |    +--- org.apache.xmlgraphics:batik-ext:1.8 (*)
|    |    |    |    +--- org.apache.xmlgraphics:batik-util:1.8
|    |    |    |    +--- org.apache.xmlgraphics:batik-xml:1.8
|    |    |    |    |    \--- org.apache.xmlgraphics:batik-util:1.8
|    |    |    |    +--- xalan:xalan:2.7.0
|    |    |    |    |    \--- xml-apis:xml-apis:2.0.2 -> 1.4.01
|    |    |    |    +--- xml-apis:xml-apis:1.3.04 -> 1.4.01
|    |    |    |    \--- xml-apis:xml-apis-ext:1.3.04
|    |    |    +--- org.apache.xmlgraphics:batik-ext:1.8 (*)
|    |    |    +--- org.apache.xmlgraphics:batik-parser:1.8
|    |    |    |    +--- org.apache.xmlgraphics:batik-awt-util:1.8 (*)
|    |    |    |    +--- org.apache.xmlgraphics:batik-util:1.8
|    |    |    |    +--- org.apache.xmlgraphics:batik-xml:1.8 (*)
|    |    |    |    \--- xml-apis:xml-apis:1.3.04 -> 1.4.01
|    |    |    +--- org.apache.xmlgraphics:batik-util:1.8
|    |    |    +--- xml-apis:xml-apis:1.3.04 -> 1.4.01
|    |    |    \--- xml-apis:xml-apis-ext:1.3.04
|    |    +--- org.apache.xmlgraphics:batik-bridge:1.8
|    |    |    +--- org.apache.xmlgraphics:batik-anim:1.8
|    |    |    |    +--- org.apache.xmlgraphics:batik-awt-util:1.8 (*)
|    |    |    |    +--- org.apache.xmlgraphics:batik-css:1.8 (*)
|    |    |    |    +--- org.apache.xmlgraphics:batik-dom:1.8 (*)
|    |    |    |    +--- org.apache.xmlgraphics:batik-ext:1.8 (*)
|    |    |    |    +--- org.apache.xmlgraphics:batik-parser:1.8 (*)
|    |    |    |    +--- org.apache.xmlgraphics:batik-svg-dom:1.8 (*)
|    |    |    |    +--- org.apache.xmlgraphics:batik-util:1.8
|    |    |    |    +--- xml-apis:xml-apis:1.3.04 -> 1.4.01
|    |    |    |    \--- xml-apis:xml-apis-ext:1.3.04
|    |    |    +--- org.apache.xmlgraphics:batik-awt-util:1.8 (*)
|    |    |    +--- org.apache.xmlgraphics:batik-css:1.8 (*)
|    |    |    +--- org.apache.xmlgraphics:batik-dom:1.8 (*)
|    |    |    +--- org.apache.xmlgraphics:batik-ext:1.8 (*)
|    |    |    +--- org.apache.xmlgraphics:batik-gvt:1.8
|    |    |    |    +--- org.apache.xmlgraphics:batik-awt-util:1.8 (*)
|    |    |    |    \--- org.apache.xmlgraphics:batik-util:1.8
|    |    |    +--- org.apache.xmlgraphics:batik-parser:1.8 (*)
|    |    |    +--- org.apache.xmlgraphics:batik-script:1.8
|    |    |    |    +--- org.apache.xmlgraphics:batik-anim:1.8 (*)
|    |    |    |    +--- org.apache.xmlgraphics:batik-util:1.8
|    |    |    |    \--- xml-apis:xml-apis:1.3.04 -> 1.4.01
|    |    |    +--- org.apache.xmlgraphics:batik-svg-dom:1.8 (*)
|    |    |    +--- org.apache.xmlgraphics:batik-util:1.8
|    |    |    +--- org.apache.xmlgraphics:batik-xml:1.8 (*)
|    |    |    +--- xalan:xalan:2.7.0 (*)
|    |    |    +--- xml-apis:xml-apis:1.3.04 -> 1.4.01
|    |    |    \--- xml-apis:xml-apis-ext:1.3.04
|    |    +--- org.apache.xmlgraphics:batik-awt-util:1.8 (*)
|    |    +--- org.apache.xmlgraphics:batik-gvt:1.8 (*)
|    |    +--- org.apache.xmlgraphics:batik-transcoder:1.8
|    |    |    +--- org.apache.xmlgraphics:batik-anim:1.8 (*)
|    |    |    +--- org.apache.xmlgraphics:batik-awt-util:1.8 (*)
|    |    |    +--- org.apache.xmlgraphics:batik-bridge:1.8 (*)
|    |    |    +--- org.apache.xmlgraphics:batik-dom:1.8 (*)
|    |    |    +--- org.apache.xmlgraphics:batik-gvt:1.8 (*)
|    |    |    +--- org.apache.xmlgraphics:batik-svggen:1.8
|    |    |    |    +--- org.apache.xmlgraphics:batik-awt-util:1.8 (*)
|    |    |    |    +--- org.apache.xmlgraphics:batik-util:1.8
|    |    |    |    \--- xml-apis:xml-apis:1.3.04 -> 1.4.01
|    |    |    +--- org.apache.xmlgraphics:batik-util:1.8
|    |    |    +--- org.apache.xmlgraphics:batik-xml:1.8 (*)
|    |    |    +--- xml-apis:xml-apis:1.3.04 -> 1.4.01
|    |    |    \--- xml-apis:xml-apis-ext:1.3.04
|    |    +--- org.apache.xmlgraphics:batik-extension:1.8
|    |    |    +--- org.apache.xmlgraphics:batik-anim:1.8 (*)
|    |    |    +--- org.apache.xmlgraphics:batik-awt-util:1.8 (*)
|    |    |    +--- org.apache.xmlgraphics:batik-bridge:1.8 (*)
|    |    |    +--- org.apache.xmlgraphics:batik-css:1.8 (*)
|    |    |    +--- org.apache.xmlgraphics:batik-dom:1.8 (*)
|    |    |    +--- org.apache.xmlgraphics:batik-ext:1.8 (*)
|    |    |    +--- org.apache.xmlgraphics:batik-gvt:1.8 (*)
|    |    |    +--- org.apache.xmlgraphics:batik-parser:1.8 (*)
|    |    |    +--- org.apache.xmlgraphics:batik-svg-dom:1.8 (*)
|    |    |    +--- org.apache.xmlgraphics:batik-util:1.8
|    |    |    +--- xml-apis:xml-apis:1.3.04 -> 1.4.01
|    |    |    \--- xml-apis:xml-apis-ext:1.3.04
|    |    +--- org.apache.xmlgraphics:batik-ext:1.8 (*)
|    |    +--- commons-logging:commons-logging:1.0.4 -> 1.1.1
|    |    +--- commons-io:commons-io:1.3.1
|    |    +--- avalon-framework:avalon-framework-api:4.2.0
|    |    \--- avalon-framework:avalon-framework-impl:4.2.0
|    +--- jdom:jdom:1.1
|    |    \--- org.jdom:jdom:1.1 -> 1.1.3
|    +--- log4j:log4j:1.2.17
|    +--- net.java.dev.swing-layout:swing-layout:1.0.2
|    +--- com.jgoodies:jgoodies-looks:2.7.0
|    |    \--- com.jgoodies:jgoodies-common:1.8.1
|    +--- org.glycoinfo.eurocarbdb.depends:DJNativeSwing-SWT:0.2
|    +--- org.glycoinfo.eurocarbdb.depends:DJNativeSwing:0.1
|    +--- org.glycoinfo.eurocarbdb.depends:flamingo-tst:0.1
|    +--- org.eurocarbdb:resourcesdb:0.4 (*)
|    +--- org.glycoinfo.convert:glycanformatconverter:2.5.2
|    |    +--- commons-collections:commons-collections:20040616
|    |    +--- org.json:json:20180813
|    |    +--- org.eurocarbdb:MolecularFramework:0.1-b37-SNAPSHOT (*)
|    |    \--- org.glycoinfo:wurcsframework:1.0.1 (*)
|    \--- org.apache.ant:ant-launcher:1.9.7
\--- junit:junit:4.12 (*)

testRuntimeOnly - Runtime only dependencies for source set 'test'. (n)
No dependencies

(*) - dependencies omitted (listed previously)

(n) - Not resolved (configuration is not meant to be resolved)

A web-based, searchable dependency report is available by adding the --scan option.

Deprecated Gradle features were used in this build, making it incompatible with Gradle 7.0.
Use '--warning-mode all' to show the individual deprecation warnings.
See https://docs.gradle.org/6.6/userguide/command_line_interface.html#sec:command_line_warnings

BUILD SUCCESSFUL in 2s
1 actionable task: 1 executed
```