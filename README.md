# wurcs2pic

## wurcs2pic is a tool for generating image data from WURCS strings and uses the features of GlycanBuilder.

  * WURCS: *J. Chem. Inf. Model.* 2017, 57, 4, 632–637. (https://doi.org/10.1021/acs.jcim.6b00650)

  * GlycanBuilder: *Carbohydrate Research*, Volume 445, 5 June 2017, Pages 104-116.  (https://doi.org/10.1016/j.carres.2017.04.015)


## Release notes

* 2024/8/27: released ver 0.16.2
  * Add option of reducing end

* 2024/8/26: released ver 0.16.1
  * Changed GlycanBuilder2 version 1.25.3
  * Changed MolecularFramework version 1.0.0
  * Changed GlycanFormatConverter version 2.10.4

* 2022/12/28: released ver 0.16.0
  * Changed GlycanBuilder2 version 1.20.1

* 2022/11/18: released ver 0.15.0
  * Changed GlycanBuilder2 version 1.20.0

* 2022/10/12: released ver 0.14.0
  * Changed GlycanBuilder2 version 1.19.0

* 2022-01-20: released ver 0.13.0
   * Changed GlycanBuilder2 version 1.15.0


## Usege

###  **WURCS strings should be enclosed in single quotes ('), not double quotes (").**


```
$ java -jar wurcs2pic.jar {png|svg|base64} '{WURCS string}' {option: output file name|path} {option: notation: SNFG|IUPAC} {option: reducing end: true|false}
```


## How to build


* gradle

```
$ gradle -v

------------------------------------------------------------
Gradle 7.5.1
------------------------------------------------------------

Build time:   2022-08-05 21:17:56 UTC
Revision:     d1daa0cbf1a0103000b71484e1dbfe096e095918

Kotlin:       1.6.21
Groovy:       3.0.10
Ant:          Apache Ant(TM) version 1.10.11 compiled on July 10 2021
JVM:          17.0.4.1 (Homebrew 17.0.4.1+1)
OS:           Mac OS X 12.6 aarch64
```

* Maven

```
mvn -v
Apache Maven 3.8.6 (84538c9988a25aec085021c365c560670ad80f63)
Maven home: /opt/homebrew/Cellar/maven/3.8.6/libexec
Java version: 19, vendor: Homebrew, runtime: /opt/homebrew/Cellar/openjdk/19/libexec/openjdk.jdk/Contents/Home
Default locale: ja_JP, platform encoding: UTF-8
OS name: "mac os x", version: "12.6", arch: "aarch64", family: "mac"
```



```
cd /Users/{User}/WURCS2PIC
```

```
git clone https://gitlab.com/glycoinfo/wurcs2pic.git
```

```
Cloning into 'wurcs2pic'...
remote: Enumerating objects: 34, done.
remote: Counting objects: 100% (34/34), done.
remote: Compressing objects: 100% (21/21), done.
remote: Total 485 (delta 11), reused 26 (delta 8), pack-reused 451
Receiving objects: 100% (485/485), 269.52 MiB | 13.92 MiB/s, done.
Resolving deltas: 100% (143/143), done.
Updating files: 100% (56/56), done.
```

### gradle

```
cp gradle.properties.template gradle.properties
```
```
vi gradle.properties
```

* add username and token of github in gradle.properties

  * https://docs.github.com/en/github/authenticating-to-github/creating-a-personal-access-token

```
cat gradle.properties
```
```
username={ your username}
token={your token}
```

```
gradle shadowJar
```

* SVG

```
java -jar ./build/libs/wurcs2pic-0.16.0-all.jar svg 'WURCS=2.0/4,6,5/[a2122h-1b_1-5_2*NCC/3=O][a1122h-1b_1-5][a1122h-1a_1-5][a1221m-1a_1-5]/1-1-2-3-3-4/a4-b1_a6-f1_b4-c1_c3-d1_c6-e1' outputname
```

```
$ ls
outputname.svg
```

* PNG

```
java -jar ./build/libs/wurcs2pic-0.16.0-all.jar png 'WURCS=2.0/4,6,5/[a2122h-1b_1-5_2*NCC/3=O][a1122h-1b_1-5][a1122h-1a_1-5][a1221m-1a_1-5]/1-1-2-3-3-4/a4-b1_a6-f1_b4-c1_c3-d1_c6-e1' outputname false
```

```
$ ls
outputname.png
```
### maven

```
mvn clean compile assembly:single
```

* SVG

```
java -jar ./target/wurcs2pic.jar svg 'WURCS=2.0/4,6,5/[a2122h-1b_1-5_2*NCC/3=O][a1122h-1b_1-5][a1122h-1a_1-5][a1221m-1a_1-5]/1-1-2-3-3-4/a4-b1_a6-f1_b4-c1_c3-d1_c6-e1' outputname SNFG false
```

```
java -jar ./target/wurcs2pic.jar svg 'WURCS=2.0/4,8,7/[a2122h-1b_1-5_2*NCC/3=O][a1122h-1b_1-5][a1122h-1a_1-5][a2112h-1a_1-5]/1-1-2-3-1-3-1-4/a4-b1_b4-c1_c3-d1_c6-f1_d4-e1_f4-g1_h1-a4|b4|c4|d4|e4|f4|g4}' outputname
```



```
$ ls
outputname.svg
```


* PNG

```
java -jar ./target/wurcs2pic.jar png 'WURCS=2.0/4,6,5/[a2122h-1b_1-5_2*NCC/3=O][a1122h-1b_1-5][a1122h-1a_1-5][a1221m-1a_1-5]/1-1-2-3-3-4/a4-b1_a6-f1_b4-c1_c3-d1_c6-e1' outputname SNFG false
```

```
java -jar ./target/wurcs2pic.jar png 'WURCS=2.0/4,8,7/[a2122h-1b_1-5_2*NCC/3=O][a1122h-1b_1-5][a1122h-1a_1-5][a2112h-1a_1-5]/1-1-2-3-1-3-1-4/a4-b1_b4-c1_c3-d1_c6-f1_d4-e1_f4-g1_h1-a4|b4|c4|d4|e4|f4|g4}' outputname
```


```
$ ls
outputname.png
```

## Usage

### Version

```
java -jar ./target/wurcs2pic.jar -v
```

```
java -jar ./build/libs/wurcs2pic-0.16.0-all.jar -v
```

 or
```
java -jar ./target/wurcs2pic.jar -V
```

```
java -jar ./build/libs/wurcs2pic-0.16.0-all.jar -V
```


* result
```
wurcs2pic version:0.16.1
```



### base64 encoded image

* $ java -jar wurcs2pic-0.16.0-all.jar base64 '{WURCS string}'

```
java -jar ./build/libs/wurcs2pic-0.16.0-all.jar base64 'WURCS=2.0/2,2,1/[a2122h-1b_1-5][a2112h-1b_1-5]/1-2/a4-b1'
```

```
java -jar ./build/libs/wurcs2pic-0.16.0-all.jar base64 'WURCS=2.0/2,2,1/[a2122h-1b_1-5][a2112h-1b_1-5]/1-2/a4-b1'
```

* result
```
iVBORw0KGgoAAAANSUhEUgAAAi4AAAD2CAIAAAB+2pvjAAAU2ElEQVR4 ...
```

## Important note:

***Do not add an extension to the output file name in the run command. The software adds the extension to the output file name of the input command. If you do not specify the output file name, the output file name will be 'wurcs2pic{.png or svg}'.***


### png image

* $ java -jar ./build/libs/wurcs2pic-0.16.0-all.jar png '{WURCS string}}' {output png file name/path}

  * If you do not specify a file name, the file will be created with the default name.


```
java -jar ./build/libs/wurcs2pic-0.16.0-all.jar png 'WURCS=2.0/2,2,1/[a2122h-1b_1-5][a2112h-1b_1-5]/1-2/a4-b1' glycan-image
```
* resiult
```
$ ls
$ glycan-image.png
```

```
mkdir img
```

```
java -jar ./build/libs/wurcs2pic-0.16.0-all.jar png 'WURCS=2.0/2,2,1/[a2122h-1b_1-5][a2112h-1b_1-5]/1-2/a4-b1' './img/glycan-image'
```
* result
```
$ cd img
$ ls
$ glycan-image.png
```

```
java -jar ./build/libs/wurcs2pic-0.16.0-all.jar png 'WURCS=2.0/2,2,1/[a2122h-1b_1-5][a2112h-1b_1-5]/1-2/a4-b1'
```

* result
```
$ ls
$ {SHA512 Hash string of WURCS}.png # default name
```

```
java -jar ./build/libs/wurcs2pic-0.16.0-all.jar png 'WURCS=2.0/2,2,1/[a2122h-1b_1-5][a2112h-1b_1-5]/1-2/a4-b1'
```

* result
```
a38560456d22c8a6c5d5a5ac8fd334da46629513f78e74c3cec0ba63e247de1d453238cedc6150ea35519b3bc590a2f1be4d65e5eb8c14a063467aba268030cc.png
```

* sample of png image

[PNG image](https://gitlab.com/glycoinfo/wurcs2pic/-/raw/master/jar_file/wurcs2pic.png)



### svg image

* $ java -jar wurcs2pic-0.16.0-all.jar svg '{WURCS string}}' {output svg file name/path}

  * If you do not specify a file name, the file will be created with the default name.


```
java -jar ./build/libs/wurcs2pic-0.16.0-all.jar svg 'WURCS=2.0/2,2,1/[a2122h-1b_1-5][a2112h-1b_1-5]/1-2/a4-b1' glycan-image
```
* result
```
$ ls
$ glycan-image.svg
or
$ mkdir img
```

```
java -jar ./build/libs/wurcs2pic-0.16.0-all.jar svg 'WURCS=2.0/2,2,1/[a2122h-1b_1-5][a2112h-1b_1-5]/1-2/a4-b1' './img/glycan-image'
```
* result
```
$ cd img
$ ls
$ glycan-image.svg
```
or
```
java -jar ./build/libs/wurcs2pic-0.16.0-all.jar svg 'WURCS=2.0/2,2,1/[a2122h-1b_1-5][a2112h-1b_1-5]/1-2/a4-b1'
```
* result
```
$ ls
$ {SHA512 Hash string of WURCS}.svg # default name
```

```
java -jar ./build/libs/wurcs2pic-0.16.0-all.jar svg 'WURCS=2.0/2,2,1/[a2122h-1b_1-5][a2112h-1b_1-5]/1-2/a4-b1'
```

* result
```
a38560456d22c8a6c5d5a5ac8fd334da46629513f78e74c3cec0ba63e247de1d453238cedc6150ea35519b3bc590a2f1be4d65e5eb8c14a063467aba268030cc.svg
```

* sample of svg image

[svg image](https://gitlab.com/glycoinfo/wurcs2pic/-/blob/master/jar_file/wurcs2pic.svg)



### IUPAC notation image

* $ java -jar wurcs2pic-0.16.0-all.jar png '{WURCS string}}' {output png file name/path} {notation: SNFG or IUPAC}

  * If you do not specify a file name, the file will be created with the default name.
  * If you do not specify a notation, the notation will be used with the SNFG.

```
java -jar ./build/libs/wurcs2pic-0.16.0-all.jar png 'WURCS=2.0/2,2,1/[a2122h-1b_1-5][a2112h-1b_1-5]/1-2/a4-b1' glycan-image IUPAC
```
* result
```
$ ls
$ glycan-image.png
```

```
$ mkdir img
```
```
java -jar ./build/libs/wurcs2pic-0.16.0-all.jar png 'WURCS=2.0/2,2,1/[a2122h-1b_1-5][a2112h-1b_1-5]/1-2/a4-b1' './img/glycan-image' IUPAC
```

* result
```
$ cd img
$ ls
$ glycan-image.png
```
