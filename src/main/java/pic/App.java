package main.java.pic;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.util.LinkedList;

import javax.imageio.ImageIO;

import org.apache.commons.codec.binary.Base64;
import org.eurocarbdb.application.glycanbuilder.BuilderWorkspace;
import org.eurocarbdb.application.glycanbuilder.Glycan;
import org.eurocarbdb.application.glycanbuilder.massutil.MassOptions;
import org.eurocarbdb.application.glycanbuilder.renderutil.GlycanRendererAWT;
import org.eurocarbdb.application.glycanbuilder.renderutil.SVGUtils;
import org.eurocarbdb.application.glycanbuilder.util.GraphicOptions;
import org.glycoinfo.application.glycanbuilder.converterWURCS2.WURCS2Parser;


public class App {

	private static String const_soft_version = "0.16.2";

	public static byte[] getImageB(String sequence, String text, String format, boolean redend) throws Exception {
		Glycan t_glycan;
		//GlycanRenderer glycanRenderer = new GlycanRendererAWT();
		ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
		try {
			BuilderWorkspace glycanWorkspace = new BuilderWorkspace(new GlycanRendererAWT());
			glycanWorkspace.initData();
			WURCS2Parser t_wurcsparser = new WURCS2Parser();
			t_glycan = t_wurcsparser.readGlycan(sequence, new MassOptions());


			if(format.equals("SNFG")) {
				glycanWorkspace.setNotation(GraphicOptions.NOTATION_SNFG);
			}
			else if(format.equals("IUPAC")) {
				glycanWorkspace.setNotation(GraphicOptions.NOTATION_TEXT);
			}
			
			glycanWorkspace.getGlycanRenderer().getGraphicOptions().SHOW_REDEND_CANVAS = redend;
			
			BufferedImage img = glycanWorkspace.getGlycanRenderer().getImage(t_glycan, true, false, redend, 1.0D); //3.0D);
			Graphics g = img.createGraphics();
			g.setColor(Color.black);

			g.drawString(text, 10, 10);

			ImageIO.write(img, "png", byteArrayOutputStream);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return byteArrayOutputStream.toByteArray();
	}

	public static String getImage(String sequence, String format, boolean redend) throws Exception {
		Glycan t_glycan;
		String strResult = "";
		//GlycanRenderer glycanRenderer = new GlycanRendererAWT();
		try {
			//BuilderWorkspace glycanWorkspace = new BuilderWorkspace(glycanRenderer);
			BuilderWorkspace glycanWorkspace = new BuilderWorkspace(new GlycanRendererAWT());
			glycanWorkspace.initData();
			WURCS2Parser t_wurcsparser = new WURCS2Parser();
			t_glycan = t_wurcsparser.readGlycan(sequence, new MassOptions());

			if(format.equals("SNFG")) {
				glycanWorkspace.setNotation(GraphicOptions.NOTATION_SNFG);
			}
			else if(format.equals("IUPAC")) {
				glycanWorkspace.setNotation(GraphicOptions.NOTATION_TEXT);
			}

			glycanWorkspace.getGlycanRenderer().getGraphicOptions().SHOW_REDEND_CANVAS = redend;
			
			BufferedImage img = glycanWorkspace.getGlycanRenderer().getImage(t_glycan, true, false, redend, 1.0D); //3.0D);
			
      ByteArrayOutputStream bos = new ByteArrayOutputStream();
      ImageIO.write(img, "png", bos);
      byte[] bytes = bos.toByteArray();
      //byte[] sbytes = encodeBase64(bytes);

      strResult = Base64.encodeBase64String(bytes);
                //.encodeToString(bytes);

      //strResult = new String(sbytes);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return strResult;
	}

	public static String getSVG(String sequence, String imgFormat, String format, boolean redend) throws Exception {
		//GlycanRenderer glycanRenderer = new GlycanRendererAWT();
		//BuilderWorkspace glycanWorkspace = new BuilderWorkspace(glycanRenderer);
		BuilderWorkspace glycanWorkspace = new BuilderWorkspace(new GlycanRendererAWT());
		glycanWorkspace.initData();

		if(format.equals("SNFG")) {
			glycanWorkspace.setNotation(GraphicOptions.NOTATION_SNFG);
		}
		else if(format.equals("IUPAC")) {
			glycanWorkspace.setNotation(GraphicOptions.NOTATION_TEXT);
		}
		glycanWorkspace.getGlycanRenderer().getGraphicOptions().SHOW_REDEND_CANVAS = redend;
		
		Glycan t_glycan;
		LinkedList<Glycan> structures = new LinkedList<>();
		String t_svg = "";
		try {
			WURCS2Parser t_wurcsparser = new WURCS2Parser();
			t_glycan = t_wurcsparser.readGlycan(sequence, new MassOptions());
			structures.add(t_glycan);
			return SVGUtils.getVectorGraphics((GlycanRendererAWT) glycanWorkspace.getGlycanRenderer(), structures, false, redend);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return t_svg;
	}

	public static void main(String[] args) {

		String soft_version = const_soft_version; //"0.15.0";
		String str_type = "base64"; // base64 or png
		String sequence = "";
		String str_filename = "wurcs2pic";
		String strFormat = "SNFG";
		boolean redend = false; 

		if (args.length == 1) {
			str_type = args[0];
		} else if (args.length == 2) {
			str_type = args[0];
			//System.out.println("str_type = " + str_type);
			sequence = args[1];
			//System.out.println("WURCS = " + sequence);
		} else if (args.length == 3) {
			str_type = args[0];
			//System.out.println("str_type = " + str_type);
			sequence = args[1];
			//System.out.println("WURCS = " + sequence);
			str_filename = args[2];
		} else if (args.length == 4) {
			str_type = args[0];
			//System.out.println("str_type = " + str_type);
			sequence = args[1];
			//System.out.println("WURCS = " + sequence);
			str_filename = args[2];
			strFormat = args[3];
		} else if (args.length == 5) {
			str_type = args[0];
			//System.out.println("str_type = " + str_type);
			sequence = args[1];
			//System.out.println("WURCS = " + sequence);
			str_filename = args[2];
			strFormat = args[3];
			String str_redend = args[4];
			System.out.println("redEnd: " + str_redend);
			if (str_redend == "false") {redend = false;}
			else if (str_redend == "true") {redend = true;}
		}
		


		if (str_filename == "wurcs2pic") {
			str_filename = getSHA512(sequence);
		}


		if (str_type.equals("base64")) {

			try {
				wurcs2base64(sequence, strFormat, redend);
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else if (str_type.equals("png")) {
			try {
				byte[] byts = App.getImageB(sequence, "", strFormat, redend);
				wurcs2png(byts, str_filename + ".png");
			} catch (Exception e) {
				e.printStackTrace();
			}

		} else if (str_type.equals("svg")) {
			try {
				String svgString = App.getSVG(sequence, "svg", strFormat, redend);
				wurcs2svg(svgString, str_filename + ".svg");
			} catch (Exception e) {
				e.printStackTrace();
			}

		} else if (str_type.equals("-v") || str_type.equals("-V")) {
			System.out.println("wurcs2pic version:" + soft_version);
		} else {
			System.out.println("check input format");
		}
		//System.out.println("Fin.");
	}


	private static void wurcs2svg(String SVGString, String filename) {
		try {
			PrintWriter pw2 = new PrintWriter(
					new BufferedWriter(
							new OutputStreamWriter(
									new FileOutputStream
											(filename), "UTF-8")));
			pw2.print(SVGString);
			pw2.close();

			System.out.println(filename);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private static void wurcs2png(byte[] byteArray, String filename) {
		//InputStream is = new ByteArrayInputStream(byteArray);
		//	BufferedImage bi = ImageIO.read(is);
		//	ImageIO.write(bi, "png", new File(filename));
		try (FileOutputStream fos = new FileOutputStream(filename)) {
			fos.write(byteArray);
			System.out.println(filename);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private static String wurcs2base64(String sequence, String format, boolean redend) {
		String str_er = "";
		//System.out.println("wurcs2base64 seq = " + sequence);
		try {
			String str = App.getImage(sequence, format, redend);
			System.out.println(str);
			return str;
		} catch (Exception e) {
			e.printStackTrace();
			return str_er;
		} catch (StackOverflowError e) {
			e.printStackTrace();
			return str_er;
		}

	}

	public static String getSHA256(String input) {

		String toReturn = null;
		try {
			MessageDigest digest = MessageDigest.getInstance("SHA-256");
			digest.reset();
			digest.update(input.getBytes("utf8"));
			toReturn = String.format("%064x", new BigInteger(1, digest.digest()));
		} catch (Exception e) {
			e.printStackTrace();
		}

		return toReturn;
	}

	public static String getSHA512(String input) {

		String toReturn = null;
		try {
			MessageDigest digest = MessageDigest.getInstance("SHA-512");
			digest.reset();
			digest.update(input.getBytes("utf8"));
			toReturn = String.format("%0128x", new BigInteger(1, digest.digest()));
		} catch (Exception e) {
			e.printStackTrace();
		}

		return toReturn;
	}
}
